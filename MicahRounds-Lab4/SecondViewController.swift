//
//  SecondViewController.swift
//  MicahRounds-Lab4
//
//  Created by Micah Rounds on 7/12/20.
//  Copyright © 2020 Micah Rounds. All rights reserved.
//

import UIKit

class SecondViewController: UITableViewController {

    var favList: [String] = []
   
    @IBOutlet weak var favTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let favs = UserDefaults.standard
        let movieList = favs.object(forKey: "movieList") as? Array<String>
        if movieList != nil{
            favList = movieList!
        }
        
        // Do any additional setup after loading the view.
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      
        return favList.count
    }
    
   override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
    
        cell.accessoryType = .disclosureIndicator
        cell.editingAccessoryView = UIImageView(image: UIImage(named: "delete"))
        cell.textLabel?.text = favList[(indexPath.row)]
        return cell
    }
    /*I got some help from this youtube video regarding swipe functionality for table cells.
     youtube.com//watch?v=X01HG3UG2fk
    I used lines with the UITableViewRowAction object and (action,indexPath) in from the video  */
    override func tableView( _ tableView:UITableView, editActionsForRowAt indexPath: IndexPath)-> [UITableViewRowAction]?
    {
        let delete = UITableViewRowAction(style: .destructive, title: "Remove")
        {
            (action,indexPath) in
            self.favList.remove(at: indexPath.row)
            let favs = UserDefaults.standard
            favs.set(self.favList, forKey: "movieList")
            self.tableView.deleteRows(at: [indexPath], with: .automatic)
            
        }
        return [delete]
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let favs = UserDefaults.standard
        let movieList = favs.object(forKey: "movieList") as? Array<String>
        if movieList != nil{
            favList = movieList!
        }
        
        favTable.reloadData()
        
    }
   
    

}

