//
//  ReviewViewController.swift
//  MicahRounds-Lab4
//
//  Created by Micah Rounds on 7/16/20.
//  Copyright © 2020 Micah Rounds. All rights reserved.
//

import UIKit
import WebKit
class ReviewViewController: UIViewController, WKNavigationDelegate {

    @IBOutlet weak var reviewWebView: WKWebView!
    var rvUrl:String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        reviewWebView.navigationDelegate = self
        let rUrl = URL(string: rvUrl)!
        let rUrlRequest = URLRequest(url: rUrl)
        reviewWebView.load(rUrlRequest)
        reviewWebView.allowsBackForwardNavigationGestures = true
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
