//
//  FirstViewController.swift
//  MicahRounds-Lab4
//
//  Created by Micah Rounds on 7/12/20.
//  Copyright © 2020 Micah Rounds. All rights reserved.
//

import UIKit
import WebKit
struct APIResults:Decodable {
 let page: Int
 let total_results: Int
 let total_pages: Int
 let results: [Movie]
}
struct Movie: Decodable {
 let id: Int!
 let poster_path: String?
 let title: String
 let release_date: String
 let vote_average: Double
 let overview: String
 let vote_count:Int!
}
class FirstViewController: UIViewController, WKNavigationDelegate{


    @IBOutlet weak var data1: UILabel!
    @IBOutlet weak var data2: UILabel!
    @IBOutlet weak var data3: UILabel!
    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var searchText: UITextField!
    @IBOutlet weak var currentMovieLabel: UILabel!
    var movieString = "movie"
    var movieImageString = "default.jpg"
    var dateString = "date"
    var rating = 0.0
    var summary = ""
    var id = 0
    required init?(coder: NSCoder) {
        super.init(coder:coder)
        
    }
    override func viewDidLoad() {
        var mSR:[String] = []
        var ims:[String?] = []
        var rels:[String] = []
        var avg:[Double] = []
        var desc:[String] = []
        var ids:[Int] = []
        let url = URL(string: "https://api.themoviedb.org/3/trending/movie/day?api_key=24fb559ce4228e52337e586c512da37b")
        if url != nil
        {
            if let data = try? Data(contentsOf: url!)
            //print(data)
            {
                if let mycvc1 = self.children.first as? MyCVC {
                    mycvc1.indicator.startAnimating()
                }
                let json = try? JSONDecoder().decode(APIResults.self, from: data)
                if let mycvc2 = self.children.first as? MyCVC {
                    mycvc2.indicator.stopAnimating()
                }
                //print(json)
                if let res  = json?.results
                {
                    for m in res{
                        mSR.append(m.title)
                        ims.append(m.poster_path)
                        rels.append(m.release_date)
                        avg.append(m.vote_average)
                        desc.append(m.overview)
                        ids.append(m.id)
                    }
                }
                
            }
        }
        if let cvc = self.children.first as? MyCVC {
            cvc.items = mSR
            cvc.imgs = ims
            cvc.dates = rels
            cvc.ratings = avg
            cvc.descriptions = desc
            cvc.movId = ids
            cvc.collectionView.reloadData()
            
        }
        super.viewDidLoad()
        currentMovieLabel?.text = movieString
        data1?.text = "Release date: "+dateString
        data3?.text = "Rating: "+String(rating)
        data2?.text = summary
        data2?.numberOfLines = 10
        currentMovieLabel?.numberOfLines = 3
        let imageUrl = "https://image.tmdb.org/t/p/w1280"+movieImageString
        let iUrl = URL(string: imageUrl)
        if let mycvc3 = self.children.first as? MyCVC {
            mycvc3.indicator.startAnimating()
        }
        DispatchQueue.global().async {
            do{
                let imageData = try? Data(contentsOf: iUrl!)
                DispatchQueue.main.async {
                    self.movieImageView?.image = UIImage(data: imageData!)
                    if self.currentMovieLabel != nil{
                        self.view.bringSubviewToFront(self.currentMovieLabel)
                        if let mycvc4 = self.children.first as? MyCVC {
                            mycvc4.indicator.stopAnimating()
                        }
                    }
                }
            }
            
        }
    }
   
    @IBAction func makeSearch(_ sender: Any) {
        var mSR:[String] = []
        var ims:[String?] = []
        var rels:[String] = []
        var avg:[Double] = []
        var desc:[String] = []
        var mId:[Int] = []
        //format search to handle spaces
        let search = searchText.text?.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        if (search != "")
        {
            let url = URL(string: "https://api.themoviedb.org/3/search/movie?api_key=24fb559ce4228e52337e586c512da37b&language=en-US&query=%20"+search!+"&page=1&include_adult=false")
            if url != nil
            {
                if let mycvc5 = self.children.first as? MyCVC {
                    mycvc5.indicator.startAnimating()
                }
                if let data = try? Data(contentsOf: url!)
                {
                    let json = try? JSONDecoder().decode(APIResults.self, from: data)
                    if let mycvc6 = self.children.first as? MyCVC {
                        mycvc6.indicator.startAnimating()
                    }
                    if let res  = json?.results
                    {
                        for m in res{
                            mSR.append(m.title)
                            ims.append(m.poster_path)
                            rels.append(m.release_date)
                            avg.append(m.vote_average)
                            desc.append(m.overview)
                            mId.append(m.id)
                        }
                    }
                    
                }
            }
        }
        else{
            let url = URL(string: "https://api.themoviedb.org/3/trending/movie/day?api_key=24fb559ce4228e52337e586c512da37b")
            if url != nil
            {
                if let mycvc7 = self.children.first as? MyCVC {
                    mycvc7.indicator.startAnimating()
                }
                if let data = try? Data(contentsOf: url!)
                {
                    let json = try? JSONDecoder().decode(APIResults.self, from: data)
                    if let mycvc8 = self.children.first as? MyCVC {
                        mycvc8.indicator.startAnimating()
                    }
                    if let res  = json?.results
                    {
                        for m in res{
                            mSR.append(m.title)
                            ims.append(m.poster_path)
                            rels.append(m.release_date)
                            avg.append(m.vote_average)
                            desc.append(m.overview)
                            mId.append(m.id)
                        }
                    }
                }
            }
        }
        if let cvc = self.children.first as? MyCVC {
            cvc.items = mSR
            cvc.imgs = ims
            cvc.dates = rels
            cvc.ratings = avg
            cvc.descriptions = desc
            cvc.movId = mId
            cvc.collectionView.reloadData()
        }
    }
    
    @IBAction func addFavorite(_ sender: Any) {
        let favs = UserDefaults.standard
        var movieList = favs.object(forKey: "movieList") as? Array<String>
        if (movieList?.contains(movieString))==false
        {
            movieList!.append(movieString)
            favs.set(movieList, forKey: "movieList")
        }
        else if movieList == nil
        {
            movieList = [movieString]
            favs.set(movieList, forKey: "movieList")
        }
        
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toCVC"{
        let mCvc = segue.destination as? MyCVC
        var mSR:[String] = []
        var ims:[String?] = []
        var rels:[String] = []
        var avg:[Double] = []
        var desc:[String] = []
        var ids:[Int] = []
        let recUrl = "https://api.themoviedb.org/3/movie/"+String(id)+"/recommendations?api_key=24fb559ce4228e52337e586c512da37b&language=en-US&page=1"
        let myUrl = URL(string: recUrl)
        if myUrl != nil
        {
            if let data = try? Data(contentsOf: myUrl!)
            {
                let json = try? JSONDecoder().decode(APIResults.self, from: data)
                if let res  = json?.results
                {
                    for m in res{
                        mSR.append(m.title)
                        ims.append(m.poster_path)
                        rels.append(m.release_date)
                        avg.append(m.vote_average)
                        desc.append(m.overview)
                        ids.append(m.id)
                    }
                }
            }
        }
        mCvc?.items = mSR
        mCvc?.imgs = ims
        mCvc?.dates = rels
        mCvc?.ratings = avg
        mCvc?.descriptions = desc
        mCvc?.movId = ids
        mCvc?.collectionView.reloadData()
        }
        else if segue.identifier == "gotoreview"
        {
            let rV = segue.destination as? ReviewViewController
            rV?.rvUrl = "https://www.themoviedb.org/movie/"+String(id)+"/reviews?language=en-US"
            
        }
        else if segue.identifier == "gototrailer"
        {
            let tV = segue.destination as? TrailerViewController
            tV?.movId = id
        }
    }
    
}

