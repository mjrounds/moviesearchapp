//
//  MyCVC.swift
//  MicahRounds-Lab4
//
//  Created by Micah Rounds on 7/12/20.
//  Copyright © 2020 Micah Rounds. All rights reserved.
//

import UIKit

private let reuseIdentifier = "myCell"

class MyCVC: UICollectionViewController,UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var indicator: UIActivityIndicatorView!
    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        // Register cell classes
        self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)

        // Do any additional setup after loading the view.
    }


    

    var items:[String] = []
    var imgs:[String?] = []
    var dates:[String] = []
    var ratings:[Double] = []
    var descriptions:[String] = []
    var movId:[Int] = []
    var selectMovie = ""
    var selectImage = ""
    var selectRating = 0.0
    var selectRelease = ""
    var selectOverView = ""
    var selectId = 0
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
          return items.count
    }
      override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
      {
              collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "myCell")
              let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "myCell", for: indexPath)
              cell.subviews.first?.removeFromSuperview()//prevents textviews from overlapping
              let textView = CGRect(x: 0, y: 75, width: 100, height: 25)
              let cellText = UILabel(frame: textView)
              cellText.text = items[indexPath.item]
              cellText.adjustsFontSizeToFitWidth = true
              cellText.textColor = .white
              cellText.backgroundColor = UIColor.black.withAlphaComponent(0.65)
              if let imgItem = imgs[indexPath.item]
              {
                let imageUrl = "https://image.tmdb.org/t/p/w1280"+imgItem
                let url = URL(string: imageUrl)
                indicator.startAnimating()
                DispatchQueue.global().async {
                    do{
                        let imageData = try? Data(contentsOf: url!)
                        DispatchQueue.main.async {
                            let imView = UIImageView()
                            imView.image = UIImage(data: imageData!)
                            imView.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
                            cell.addSubview(imView)
                            cell.addSubview(cellText)
                            self.indicator.stopAnimating()
                        }
                    }
                    
                }
              }
              return cell

      }
      override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            selectMovie = items[indexPath.item]
            selectRelease = dates[indexPath.item]
            selectRating = ratings[indexPath.item]
            selectOverView = descriptions[indexPath.item]
        selectId = movId[indexPath.item]
            if imgs[indexPath.item] != nil
            {
                selectImage = imgs[indexPath.item]!
            }
            performSegue(withIdentifier: "gotomovie", sender: Any?.self )
          
      }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let fVC = segue.destination as? FirstViewController
        fVC?.movieString = selectMovie
        fVC?.movieImageString = selectImage
        fVC?.dateString = selectRelease
        fVC?.rating = selectRating
        fVC?.summary = selectOverView
        fVC?.id = selectId
        
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 100)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 15, bottom: 220, right: 15)
    }
   

}
    

