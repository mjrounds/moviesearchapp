//
//  TrailerViewController.swift
//  MicahRounds-Lab4
//
//  Created by Micah Rounds on 7/16/20.
//  Copyright © 2020 Micah Rounds. All rights reserved.
//

import UIKit
import WebKit
struct Results:Decodable
{
    let id:Int
    let results:[Trailer]
}
struct Trailer:Decodable {
    let key:String
}
class TrailerViewController: UIViewController,WKNavigationDelegate {

    @IBOutlet weak var movieWebView: WKWebView!
    var movId:Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        movieWebView.navigationDelegate = self
        let tUrl = "https://api.themoviedb.org/3/movie/"+String(movId)+"/videos?api_key=24fb559ce4228e52337e586c512da37b&language=en-US"
        let trailerUrl = URL(string: tUrl)
        if trailerUrl != nil
        {
            if let data = try? Data(contentsOf: trailerUrl!)
            {
                let json = try? JSONDecoder().decode(Results.self, from: data)
                if let res  = json?.results.first
                {
                    let resultKey = res.key
                    let webUrl = "https://www.youtube.com/watch?v="+resultKey
                    if let webUrlObj = URL(string: webUrl)
                    {
                               let movReq = URLRequest(url: webUrlObj)
                               movieWebView.load(movReq)
                               movieWebView.allowsBackForwardNavigationGestures = true
                    }
                }
            }
        }
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
